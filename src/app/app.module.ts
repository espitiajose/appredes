import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { ProgramasPage } from '../pages/programas/programas';
import { AboutmePage } from '../pages/aboutme/aboutme';
import { ContactoPage } from '../pages/contacto/contacto';
import { RememberPage } from '../pages/remember/remember';
import { ComprasPage } from '../pages/compras/compras';
import { EnvivoPage } from '../pages/envivo/envivo';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    LoginPage,
    ProgramasPage,
    AboutmePage,
    ContactoPage,
    RememberPage,
    ComprasPage,
    EnvivoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    LoginPage,
    ProgramasPage,
    AboutmePage,
    ContactoPage,
    RememberPage,
    ComprasPage, 
    EnvivoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
