import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ComprasPage } from '../compras/compras';

@Component({
  selector: 'page-programas',
  templateUrl: 'programas.html',
})
export class ProgramasPage {

  constructor(public navCtrl: NavController) {
  }

  slideData = [{ image: "../../assets/imgs/Imagen1.jpeg" },{ image: "../../assets/imgs/Imagen2.jpg" },{ image: "../../assets/imgs/Imagen3.jpeg" }]
  slideData2 = [{ image2: "../../assets/imgs/Imagen1.jpeg" },{ image2: "../../assets/imgs/Imagen2.jpg" },{ image2: "../../assets/imgs/Imagen3.jpeg" }]
  slideData3 = [{ image3: "../../assets/imgs/Imagen1.jpeg" },{ image3: "../../assets/imgs/Imagen2.jpg" },{ image3: "../../assets/imgs/Imagen3.jpeg" }]
  
  goComprasPage():void{
    this.navCtrl.push(ComprasPage);
  }

}
