import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { RememberPage } from '../remember/remember';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController) {
  
  }

  goSignUpPage():void{
    this.navCtrl.push(SignupPage);
  }

  goRememberPage():void{
    this.navCtrl.push(RememberPage);
  }
}