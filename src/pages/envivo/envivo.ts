import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-envivo',
  templateUrl: 'envivo.html',
})
export class EnvivoPage {

  constructor(public navCtrl: NavController) {
  }

}
